# Nvchad Custom

My custom options and plugins to nvchad.

- To add this as local user:

`git clone https://gitlab.com/aleixq/nvchad_custom.git ~/.config/nvim/lua/custom --depth 1`

- To add this system wide:

`git clone https://gitlab.com/aleixq/nvchad_custom.git /etc/xdg/nvim/NvChad/lua/custom --depth 1`


Each time plugins are added/ suppressed, run

```
:Lazy sync
```
